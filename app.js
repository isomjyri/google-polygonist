class App {
    constructor() {
        document.addEventListener('DOMContentLoaded', () => {
            this.cache();
            this.initMap();
            this.events();
        });
    }

    /**
     * Cache commonly used elements.
     */
    cache() {
        this.coordinates = [];
        this.map = null;
        this.polygon = null;
        this.data = document.getElementById('js-data');
    }

    /**
     * Initialize map.
     */
    initMap() {
        this.map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 25.774, lng: -80.190},
            zoom: 8,
            disableDefaultUI: true,
            clickableIcons: false,
        });

        this.polygon = new google.maps.Polygon({
            strokeColor: '#F7567C',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#36413E',
            fillOpacity: 0.30
        });
    }

    /**
     * Handle events.
     */
    events() {
        // Add new polygon point.
        this.map.addListener('click', (e) => {
            this.coordinates.push({
                lat: e.latLng.lat(),
                lng: e.latLng.lng(),
            });

            this.updatePolygon(this.coordinates, this.map);
        });

        // Undo last polygon point.
        document.getElementById('js-undo').addEventListener('click', (e) => {
            this.coordinates.pop();
            this.updatePolygon(this.coordinates, this.map);
        });

        // Clear all polygons.
        document.getElementById('js-clear').addEventListener('click', (e) => {
            this.coordinates = [];
            this.updatePolygon(this.coordinates, null);
        });
        
        // Show polygon data.
        document.getElementById('js-toggle').addEventListener('click', (e) => {
            this.data.classList.toggle('data--open');
        });

        // Copy data to clipboard.
        document.getElementById('js-copy').addEventListener('click', (e) => {
            this.copyElementContent(this.data);
        });
    }

    /**
     * Update polygon.
     * 
     * @param {array} coordinates Polygon coordinates.
     * @param {object} map Google maps object.
     */
    updatePolygon(coordinates, map) {
        if(this.coordinates.length) {
            this.data.innerHTML = JSON.stringify(this.coordinates).replace( /"lat":|"lng":/g, '');
        } else {
            this.data.innerHTML = '';
        }

        this.polygon.setPath(coordinates);
        this.polygon.setMap(map);
    }

    /**
     * Copy element content.
     * 
     * @param {element} elem Element to copy.
     */
    copyElementContent(elem) {
        let range = null;
        let selection = null;

        if(document.body.createTextRange) {
            range = document.body.createTextRange();
            range.moveToElementText(elem);
            range.select();
        } else {
            selection = window.getSelection();
            range = document.createRange();
            range.selectNodeContents(elem);
            selection.removeAllRanges();
            selection.addRange(range);
        }

        document.execCommand("Copy");
    }
}

new App();